import requests
def weather(place):
    try:
        url = 'http://api.openweathermap.org/data/2.5/weather?q=' + place + '&APPID=c37f55bec299c73b323147a2dbe32031&units=metric'
        r = requests.get(url,timeout=10)
        j = r.json()
        name = j['name']
        temp = j['main']['temp']
        wind = j['wind']['speed']
        weather = j['weather'][0]
        description = weather['description']
        main = weather['main']
        return(name + ": " +  str(temp) + " °C" + " | "+ "Wind: " + str(wind) + " km/h" + " | " +  str(main))
    except:
        pass