import requests

def quran(ayah):
    try:
        ayah = str(ayah)
        url = 'http://api.alquran.cloud/ayah/' + ayah + '/en.asad'
        r = requests.get(url,timeout=10)
        j = r.json()
        text = j['data']['text']
        return text
    except :
        return ("An error occured.")

