import requests

def bible(verse):
    try:
        ayah = str(verse)
        url = 'https://bible-api.com/'+ verse
        r = requests.get(url,timeout=10)
        j = r.json()
        text = j['text']
        return text
    except:
        pass

