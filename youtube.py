import urllib.parse
import urllib.request
import re
from modules import url


def youtube(word):
    try:
        query_string = urllib.parse.urlencode({"search_query" : word})
        html_content = urllib.request.urlopen("http://www.youtube.com/results?" + query_string)
        search_results = re.findall(r'href=\"\/watch\?v=(.{11})', html_content.read().decode())
        title = "http://www.youtube.com/watch?v=" + search_results[0]
        return("Search result: " + url(title) + " " + "http://www.youtube.com/watch?v=" + search_results[0])
    except:
        return("An error occured")
