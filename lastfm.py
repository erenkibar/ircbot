##    This program is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    This program is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with this program.  If not, see <http://www.gnu.org/licenses/>.

import pylast
import pymongo
from pymongo import MongoClient
API_KEY = ''
API_SECRET = ''
network = pylast.LastFMNetwork(API_KEY,API_SECRET)



def user_exists(nick):
    connection = MongoClient('localhost', 27017)
    db = connection.LASTFM
    collection = db.lasftmusers

    if collection.find({nick: {'$exists':True}}).count() > 0:
        return True

def add_user(nick,username):
    '''Add user to the database'''
    connection = MongoClient('localhost', 27017)
    db = connection.LASTFM
    collection = db.lasftmusers
    if user_exists(nick):
        return("Nick " + nick +" already exists in the database!")
    else:
        collection.insert_one({nick:username})
        return("User added!")

def remove_user(nick):
    '''Remove a user from the database'''
    connection = MongoClient('localhost', 27017)
    db = connection.LASTFM
    collection = db.lasftmusers
    if user_exists(nick):
        collection.delete_one({nick:{'$exists':True}})
        return("Username deleted!")
    else:
        pass

def now_playing(nick):
    connection = MongoClient('localhost', 27017)
    db = connection.LASTFM
    collection = db.lasftmusers
    try:
        if collection.find_one({nick: { '$exists': True }}):
            post = collection.find_one({nick: {'$exists': True}})
            userlast = post[nick]
            user = network.get_user(userlast.strip(" "))
            if user.get_now_playing() == None:
                return (userlast.strip(" ") + " is not playing anything right now!") 
            else:
                track = user.get_now_playing()
                return( userlast.strip(" ") + " is playing " + str(track))
        else:
            pass
    except:
        pass


