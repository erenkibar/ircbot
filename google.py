import requests
from bs4 import BeautifulSoup
from modules import url

def google(word):
    try:
     
        goog_search = "https://www.google.com/search?q=" + word
        r = requests.get(goog_search)  
        soup = BeautifulSoup(r.text, "html.parser")
        link = soup.find('cite').text
        link = link.replace(" ","")
        return link + " | " + url(link)  
    except:
        pass
