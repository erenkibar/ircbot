import requests
from bs4 import BeautifulSoup
def url(url):
    try:
        headers = {'User-Agent': 'Mozilla/5.0'}   
        r = requests.get(url.strip("'"),headers = headers)
        html = BeautifulSoup(r.text,"lxml")
        title = html.title.text
        return title
    except:
        pass
