import requests
def urban(word):
    try:
        url = "http://api.urbandictionary.com/v0/define?term=" + word
        r = requests.get(url, timeout=10)
        print(r)
        j = r.json()['list'][0]
        print(j)
        word = j['word']
        definition = j['definition']
        example = j['example']
        pl = j['permalink'].rsplit('/', 1)[0]
        text = word.upper() + " | " + definition[0:400] + " | "  + example + " | " + pl
        return text
    except:
        pass
